public without sharing class OpenWeatherCalloutService {
    
    private static final String API_KEY = Anomaly_Range_Configuration__c.getOrgDefaults().API_Key__c ;
    private static final String ENDPOINT_URL = 'https://api.openweathermap.org/data/2.5/weather';
    private Map<String, City__c> cityCodeToCityMap;
    
    public OpenWeatherCalloutService(){
        this.cityCodeToCityMap = new Map<String, City__c>();
        for(City__c city : [SELECT Id, City_Code__c FROM City__c]){
            this.cityCodeToCityMap.put(city.City_Code__c, city);
        }
    }
    
    public void getWeatherFromOpenWeather() {
        List<Weather__c> weatherToCreate = new List<Weather__c>();
        for(City__c city : cityCodeToCityMap.values()){
            Weather__c currentWeather = makeCallout(city.City_Code__c);
            if(currentWeather != null){
                weatherToCreate.add(currentWeather);
            }
        }
        system.debug('weatherToCreate size:' + weatherToCreate.size());
        if(!weatherToCreate.isEmpty()){
            insert weatherToCreate;
        }
    }
    
    public Weather__c makeCallout(String cityId){
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(ENDPOINT_URL + '?id=' + cityId + '&appid=' + API_KEY);
        request.setMethod('GET');
        HttpResponse response = http.send(request);
        
        if(response.getStatusCode() == 200){
            OpenWeatherResponse weatherResponse = (OpenWeatherResponse)JSON.deserialize(response.getBody(), OpenWeatherResponse.class);
            return populateWeatherRecord(weatherResponse, cityId);
        } else{
            system.debug('status code: ' + response.getStatusCode());
            system.debug('status: ' + response.getStatus());
            return null;
        }
    }
    
    public Weather__c populateWeatherRecord(OpenWeatherResponse responseWrap, String cityId){
        Weather__c newWeather = new Weather__c(City__c = cityCodeToCityMap.get(cityId).Id
                                                , Visibility__c = responseWrap.visibility
                                                , Temperature__c = responseWrap.main.temp
                                                , Temperature_Feels_Like__c = responseWrap.main.feels_like
                                                , Minimal_Temperature__c = responseWrap.main.temp_min
                                                , Maximum_Temperature__c = responseWrap.main.temp_max
                                                , Pressure__c = responseWrap.main.pressure
                                                , Humidity__c = responseWrap.main.humidity);
        if(responseWrap.wind != null){
            newWeather.Wind_Speed__c = responseWrap.wind.speed;
        }
        if(responseWrap.clouds != null){
            newWeather.Cloudiness__c = responseWrap.clouds.all;
        }
        return newWeather;
    }
    
    public class OpenWeatherResponse{
        public Main main;
        public Integer visibility;//Weather__c.Visibility__c 
        public Clouds clouds;
        public Wind wind;
    }
    
    public class Main{
        public Decimal temp; //Weather__c.Temperature__c 
        public Decimal feels_like; //Weather__c.Temperature_Feels_Like__c 
        public Decimal temp_min; //Weather__c.Minimal_Temperature__c 
        public Decimal temp_max; //Weather__c.Maximum_Temperature__c 
        public Integer pressure; //Weather__c.Pressure__c 
        public Integer humidity; //Weather__c.Humidity__c 
    }
    
    public class Clouds{
        public Integer all;//Weather__c.Cloudiness__c 
    }
    
    public class Wind{
        public Decimal speed;//Weather__c.Wind_Speed__c  
    }
}